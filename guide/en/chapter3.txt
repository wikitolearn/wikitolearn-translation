Sections are a collection of chapters. They're intended to be used like a section  of a book and have their own title.

== Add a section ==
You can add a section to a course by navigating to the course main page. Click on the <code>edit course</code> link and insert the name of the section you want to add. Once done, click on <code>Save Course</code> button.

[[File:Manual_add_section.gif]]

== Rename a section ==
To rename a section, go to the course main page and click on the <code>edit course</code>link. A list of all the created sections will show up. Place the mouse on the target section and a blue pencil will appear on the right. Click on the pencil and insert the new name of the section. Once done, click on the <code>Save course</code>button.

[[File:Manual_rename_section.gif]]

== Delete a section ==
To delete a section, go to the course main page and click on the <code>edit course</code> link. A list of all the created section will show up. Place the mouse on the targeted section and a red bin will appear on the right. If you click on the bin you will notice that your section will be placed in a recycling bin. Once done, save the changes by clicking on the <code>Save course</code>button.

[[File:Manual_delete_section.gif]]

=== Restore a section ===
If you have sections in the recycling bin you can restore them by placing the mouse on the sections and clicking on the restore icon on the left of the section's name.

[[File:Manual_Restore_section.gif]]

Always remember to save the changes by clicking on the <code>Save Course</code>button.

== Move a section ==
You can change the order of the section's list by clicking on the <code>edit course</code>link. A list of sections will show up. Simply drag and drop them to swap their order. Once done, save your changes by clicking on the <code>Save course</code>button.

(add image or gif here)
